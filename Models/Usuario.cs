﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.Models
{
    public class UsuarioMetadata
    {
        [Display(Name = "Login")]
        public string UserName { get; set; }
    }
    public class Usuario: IdentityUser<Guid>
    {
        public string Nome { get; set; }
        public DateTime Nasci { get; set; }


    }
}
