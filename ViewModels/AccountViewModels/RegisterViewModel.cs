#region Using

using Identity.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#endregion

namespace Identity.ViewModels.AccountViewModels
{
    public class RegisterViewModel : Usuario
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "senha ")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirme senha")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public IEnumerable<SelectListItem> RolesList { get; set; }
    }
}
