using Identity.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;


namespace Identity.ViewModels.AccountViewModels
{
    public class PerfilViewModel
    {
        public PerfilViewModel()
        {

        }

        public PerfilViewModel(Usuario esteUsuario)
        {

            Nome = esteUsuario.Nome;
        }

        [Display(Name = "Nome Completo")]
        [Required]
        [MaxLength(80)]
        public string Nome { get; set; }

    }
}
