#region Using

using System.ComponentModel.DataAnnotations;

#endregion

namespace Identity.ViewModels.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
