﻿using Identity.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Identity.Controllers
{
    [Authorize]
    public class ControllerBaseMvc : Microsoft.AspNetCore.Mvc.Controller
    {
        protected readonly ApplicationDbContext _context;

        public ControllerBaseMvc(ApplicationDbContext context)
        {
            _context = context;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {

            //IPAddress remoteIpAddress = HttpContext.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress;

            //string _retorno = "";
            //var UserGuid = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
            //if (UserGuid != null)
            //{
            //    var user = _context.Users.Where(w => w.Id == Guid.Parse(UserGuid)).FirstOrDefault();
            //    if (user != null)
            //    {
            //        ViewBag.NomePolicial = user.Nome;
            //        _retorno = $"{UserGuid}@{user.Nome}";
            //    }
            //    else
            //    {
            //        _retorno = "EOF Vazio";
            //    }
            //}
            //_context.UserIDNome = StringExtensions.Left(_retorno, 450);
            //_context.IP = remoteIpAddress;
            //base.OnActionExecuting(context);
        }

    }
}
